# coding=utf-8
import numpy as np
import cv2
import util


# 此代码有bug
class Knntree:
    def __init__(self, currentNonRigidMesh, currentScanMesh):
        self.currentNonRigidMesh = currentNonRigidMesh
        # self.currentScanMesh = currentScanMesh
        self.nonRigidArray = np.zeros((currentNonRigidMesh.n_vertices(), 3), np.float32)
        self.currentScanArray = np.zeros((currentScanMesh.n_vertices(), 3), np.float32)
        self.mesh = currentScanMesh
        self.k_num = 40
        for i in range(currentNonRigidMesh.n_vertices()):
            vh = currentNonRigidMesh.vertex_handle(i)
            v = currentNonRigidMesh.point(vh)
            self.nonRigidArray[i] = [v[0], v[1], v[2]]

        for i in range(currentScanMesh.n_vertices()):
            vh = currentScanMesh.vertex_handle(i)
            v = currentScanMesh.point(vh)
            self.currentScanArray[i] = [v[0], v[1], v[2]]

        # scanset = np.array(scanPoint)
        # deformedset = np.array(icpPoint)
        # result, dists = flann.nn(scanset, deformedset, 1, algorithm="kmeans", branching=32, iterations=7, checks=16)
        # for i in range(len(result)):
        #     vh0 = currentNonRigidMesh.vertex_handle(i)
        #     vh1 = currentScanMesh.vertex_handle(int(result[i]))
        #     vertexPair.append((vh0.idx(), vh1.idx()))

        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=8)
        search_params = dict(checks=50)  # or pass empty dictionary
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        self.matches = flann.knnMatch(self.nonRigidArray, self.currentScanArray, self.k_num)

    def nearest(self, pointIdx):
        vh0 = self.currentNonRigidMesh.vertex_handle(pointIdx)
        n0 = util.Vec3DToNp(self.currentNonRigidMesh.normal(vh0))
        matchIdx = self.matches[pointIdx][0].trainIdx
        max = -1
        # idxs = []
        for i in range(self.k_num):
            idx = self.matches[pointIdx][i].trainIdx
            vh1 = self.mesh.vertex_handle(idx)
            n1 = util.Vec3DToNp(self.mesh.normal(vh1))
            normalDot = np.dot(n0, n1)
            # print normalDot
            if normalDot > max:
                max = normalDot
                matchIdx = idx
            # idxs.append(idx)
        return self.mesh.vertex_handle(matchIdx)
