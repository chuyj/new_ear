import gdist
import numpy as np


class Geodesictwo:
    def __init__(self, mesh):
        self.vertexNum = mesh.n_vertices()
        self.faceNum = mesh.n_faces()
        self.edgeNum = mesh.n_edges()
        vers = []
        faces = []
        for i in range(0, self.vertexNum):
            vhi = mesh.vertex_handle(i)
            p = mesh.point(vhi)
            point = [p[0], p[1], p[2]]
            vers.append(point)
        self.vertices = np.array(vers)
        self.vertices = self.vertices.astype(np.float64)
        # print "vertices====================="
        # print self.vertices
        for i in range(0, self.faceNum):
            fh = mesh.face_handle(i)
            fvh = mesh.fv(fh)
            vh0 = fvh.next()
            vh1 = fvh.next()
            vh2 = fvh.next()

            idx0 = vh0.idx()
            idx1 = vh1.idx()
            idx2 = vh2.idx()

            face = [idx0, idx1, idx2]
            faces.append(face)
        self.triangles = np.array(faces)
        self.triangles = self.triangles.astype(np.int32)

    def seed(self, seedIdx):
        geodist = []
        # for i in range(0, self.vertexNum):
        #     dist = gdist.compute_gdist(vertices, triangles, source_indices=seedIdx, target_indices=trg)
        src = np.array([seedIdx], dtype=np.int32)
        # print "src", src
        trg = np.array([i for i in range(0, self.vertexNum)], dtype=np.int32)
        # print "trg", trg
        geodist = gdist.compute_gdist(self.vertices, self.triangles, source_indices=src, target_indices=trg)
        return geodist


# import numpy
# temp = numpy.loadtxt("flat_triangular_mesh.txt", skiprows=1)
# vertices = temp[0:121].astype(numpy.float64)
# print "vertex:".format(50, "-")
# print vertices
# triangles = temp[121:321].astype(numpy.int32)
# print "triangles:".format(50, "-")
# print triangles
# src = numpy.array([1], dtype=numpy.int32)
# print "src", src
# trg = numpy.array([2], dtype=numpy.int32)
# print "trg", trg
# res = gdist.compute_gdist(vertices, triangles, source_indices=trg, target_indices = src)
# print res
