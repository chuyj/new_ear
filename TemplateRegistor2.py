# coding=utf-8
from posemapper import Rodrigues, getMatches
import numpy as np
import chumpy as ch
import math
from openmesh import *
from nonIcp import nrIcp
import config
import cPickle as pickle
import logging
import xml.etree.ElementTree as ET
from pyflann import *
from render import renderObj
from verts import verts_core
import sys, traceback
from serialization import loadObj, readyArgument

LOG_FILENAME = './runtimelog.out'
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG)


class TemplateRigistor:
    def __init__(self):
        self.fx = -1924.47859222
        self.fy = 1924.37369622
        self.u = 319.46511037
        self.v = 239.421891747
        self.alpha = 0
        # ----------------------
        self.template = {}
        self.scanModel = {}
        # ----------------------
        self.templateMesh = TriMesh()
        self.templateVertexNum = 0
        self.templateEdgeNum = 0
        self.templateFaceNum = 0
        # ----------------------
        self.scanMesh = TriMesh()
        self.modelFileName = ""  # earscan 的路径
        # ------------------------
        self.outputPath = ""  # global r,t后的template保存路径
        # ------------------------
        self.icpPair = []
        # ------------------------
        self.tempIterPair = []
        self.colormap = np.zeros(shape=(480, 640, 3), dtype=np.float32)
        self.scanidxMap = np.zeros((480, 640), dtype=np.int)
        self.scan_depth = []
        self.scanNormalMap = np.zeros(shape=(480, 640, 3), dtype=np.float32)
        # ##################### END #######################

    def getColorMap(self):
        P = np.array([[self.fx, 0, self.u], [0, self.fy, self.v], [0, 0, 1]])
        scanPoint = []
        scanidxlist = []
        scanVertexNum = self.scanMesh.n_vertices()
        for i in range(scanVertexNum):
            vh = self.scanMesh.vertex_handle(i)
            scanidx = vh.idx()
            v = self.scanMesh.point(vh)
            point = [v[0], v[1], v[2]]
            scanPoint.append(point)
            scanidxlist.append(scanidx)
        B = np.array(scanPoint)
        B_depth = np.dot(B, P.T)
        for i in range(B_depth.shape[0]):
            z = B_depth[i][2]
            B_depth[i][0] = B_depth[i][0] / z
            B_depth[i][1] = B_depth[i][1] / z
        self.scan_depth = B_depth
        for i in range(B_depth.shape[0]):
            z = B_depth[i][2]
            X = int(B_depth[i][0])
            Y = int(B_depth[i][1])
            _x = scanPoint[i][0]
            _y = scanPoint[i][1]
            self.colormap[Y][X][0] = _x
            self.colormap[Y][X][1] = _y
            self.colormap[Y][X][2] = z
            self.scanidxMap[Y][X] = scanidxlist[i]
        print "colorMap done!"
        print "writing to 深度图上的点云......"
        with open("./EarScan/scanMapPoints.txt", "w") as f:
            for i in range(480):
                for j in range(640):
                    x = float(self.colormap[i][j][0])
                    y = float(self.colormap[i][j][1])
                    z = float(self.colormap[i][j][2])
                    f.write('(%f,%f,%f)' % (x, y, z))
                f.write('\n')
        # ################# END ######################

    def getNormalMap(self):
        scanmesh = self.scanMesh
        scanmesh.request_face_normals()
        scanmesh.request_vertex_normals()
        scanmesh.update_normals()
        print "scanmesh:update_normals done!"
        scanVertexNum = self.scanMesh.n_vertices()
        for i in range(scanVertexNum):
            vh = self.scanMesh.vertex_handle(i)
            n = scanmesh.normal(vh)
            X = int(self.scan_depth[i][0])
            Y = int(self.scan_depth[i][1])
            self.scanNormalMap[Y][X][0] = n[0]
            self.scanNormalMap[Y][X][1] = n[1]
            self.scanNormalMap[Y][X][2] = n[2]
        # ################# END ######################

    @staticmethod
    def alignFunc(mat1, mat2, R):
        return ch.linalg.norm(Rodrigues(R).dot(mat1.T).T - mat2)

    def setTemplate(self, template):
        self.template = template

    def setScanMesh(self, fileName):
        self.modelFileName = fileName
        if not read_mesh(self.scanMesh, fileName):
            print "load scan mesh error"
        else:
            print "load scan mesh from:-- ", self.modelFileName, " --successful"
        self.getColorMap()
        self.getNormalMap()

    def setScanModel(self, scanModel):
        self.scanModel = scanModel

    def readpp(self, ppFname):
        plist = []
        tree = ET.parse(ppFname)
        root = tree.getroot()
        for i in range(1, config.NUM_OF_POINTS_2 + 1):
            point = [float(root[i].get('x')), float(root[i].get('y')), float(root[i].get('z'))]
            plist.append(point)
        plist = np.array(plist)
        return plist

    def globalRT(self):
        self.scanMesh.request_face_normals()
        self.scanMesh.request_vertex_normals()
        self.scanMesh.update_normals()
        print "self.scanMesh update_normals done"
        # read template2.pp, self.modelFileName+2.pp
        template2pp = self.readpp("template/template2.pp")
        scan2pp_dep = self.readpp(self.modelFileName.replace(".obj", "2.pp"))
        n1 = config.NUM_OF_POINTS_2
        for i in range(n1):
            X = int(scan2pp_dep[i][0])
            Y = int(scan2pp_dep[i][1])
            scan2pp_dep[i][0] = self.colormap[Y][X][0]
            scan2pp_dep[i][1] = self.colormap[Y][X][1]
            scan2pp_dep[i][2] = self.colormap[Y][X][2]

        registerIterCnt = 0
        while registerIterCnt < 1:
            n2 = len(self.tempIterPair)
            n = n1 + n2
            scanPairPoints = []
            templatePairPoints = []
            for i in range(n1):
                # s_point = [float(self.scanModel['pp'][i][0]), float(self.scanModel['pp'][i][1]),
                #            float(self.scanModel['pp'][i][2])]
                # t_point = [float(self.template['pp'][i][0]), float(self.template['pp'][i][1]),
                #            float(self.template['pp'][i][2])]
                s_point = [float(scan2pp_dep[i][0]), float(scan2pp_dep[i][1]),
                           float(scan2pp_dep[i][2])]
                t_point = [float(template2pp[i][0]), float(template2pp[i][1]),
                           float(template2pp[i][2])]
                scanPairPoints.append(s_point)
                templatePairPoints.append(t_point)
            for i in range(n2):
                tidx = self.tempIterPair[i][0]
                sidx = self.tempIterPair[i][1]
                tpoint = [float(self.template['v'][tidx][0]), float(self.template['v'][tidx][1]),
                          float(self.template['v'][tidx][2])]
                spoint = [float(self.scanModel['v'][sidx][0]), float(self.scanModel['v'][sidx][1]),
                          float(self.scanModel['v'][sidx][2])]
                scanPairPoints.append(spoint)
                templatePairPoints.append(tpoint)
            scanPairPoints = np.array(scanPairPoints)
            templatePairPoints = np.array(templatePairPoints)
            self.alignTemplate(n, scanPairPoints, templatePairPoints)
            self.saveResult(registerIterCnt)
            self.setTemplateMesh()
            # --------------------------------------
            # templateOutMesh = self.templateMesh
            self.templateMesh.request_face_normals()
            self.templateMesh.request_vertex_normals()
            self.templateMesh.update_normals()
            print "templateOutMesh update_normals done"
            self.tempIterPair = self.findVertexPair(registerIterCnt)
            # ----------------------------------
            registerIterCnt += 1
        # ######  END   ##############

    def alignTemplate(self, n, scanPairPoints, templatePairPoints):
        R = ch.zeros(3)
        t = scanPairPoints[0] - templatePairPoints[0]
        # t = self.scanModel['pp'][0] - self.template['pp'][0]
        for i in range(1, n):
            t += scanPairPoints[i] - templatePairPoints[i]
            # t += (self.scanModel['pp'][i] - self.template['pp'][i])
        t = t / n
        print "t:", t
        # ch.minimize(self.alignFunc(self.template['pp'][0:n, :] + t, self.scanModel['pp'][0:n, :], R), [R])
        # ch.minimize(self.alignFunc(templatePairPoints[0:n, :] + t, scanPairPoints[0:n, :], R), [R])
        ch.minimize(self.alignFunc(templatePairPoints[0:n, :], scanPairPoints[0:n, :] - t, R), [R])
        print R, t
        # self.template['v'] = self.template['v'] + t
        self.template['v'] = Rodrigues(R).dot(self.template['v'].T).T + t
        # self.template['v'] = self.template['v'] + t
        # self.template['v'] = Rodrigues(R).dot(self.template['v'].T).T

    # save outputmesh:translate template mesh ***.obj
    def saveResult(self, registerIterCnt):
        outmesh_path = './output/'
        idx = self.modelFileName.rfind('/')
        toreplcestr = "Out" + str(registerIterCnt) + ".obj"
        outmesh_path = outmesh_path + self.modelFileName[idx + 1:].replace(".obj", toreplcestr)
        self.outputPath = outmesh_path
        with open(outmesh_path, 'w') as fp:
            # for v in self.result.r:
            for v in self.template['v'].r:
                fp.write('v %f %f %f\n' % (v[0], v[1], v[2]))
            for f in self.template['f'] + 1:  # Faces are 1-based, not 0-based in obj files
                fp.write('f %d %d %d\n' % (f[0], f[1], f[2]))
        print '..Output mesh saved to: ', outmesh_path

    def setTemplateMesh(self):
        # self.templateMesh = TriMesh()
        if not read_mesh(self.templateMesh, self.outputPath):
            print "load registered template mesh error"
            return
        self.templateVertexNum = self.templateMesh.n_vertices()
        self.templateEdgeNum = self.templateMesh.n_edges()
        self.templateFaceNum = self.templateMesh.n_faces()
        print "load registered template mesh from:-- ", self.outputPath, " --successful"

    def setPair(self, scanname):
        scanpp = scanname.replace(".obj", "_nricp4.pp")
        templatename = './template/template.obj'
        templatepp = templatename.replace(".obj", "_nricp4.pp")
        flann = FLANN()
        # --------读取template_nricp.pp--------------
        template_plist = []
        tree = ET.parse(templatepp)
        root = tree.getroot()
        for i in range(1, config.ICP_POINTS + 1):
            point = [float(root[i].get('x')), float(root[i].get('y')), float(root[i].get('z'))]
            template_plist.append(point)
        template_pp_array = np.array(template_plist)
        templateset = np.array(self.template["v"])
        template_result, template_dists = flann.nn(templateset, template_pp_array, 1, algorithm="kmeans", branching=32,
                                                   iterations=7, checks=16)
        # --------读取earscan_nricp.pp--------------
        scan_plist = []
        tree1 = ET.parse(scanpp)
        root1 = tree1.getroot()
        for i in range(1, config.ICP_POINTS + 1):
            point = [float(root1[i].get('x')), float(root1[i].get('y')), float(root1[i].get('z'))]
            scan_plist.append(point)
        scan_pp_array = np.array(scan_plist)
        scanset = np.array(self.scanModel["v"])
        scan_result, scan_dists = flann.nn(scanset, scan_pp_array, 1, algorithm="kmeans", branching=32, iterations=7,
                                           checks=16)
        # ----------------------------------------
        for i in range(0, config.ICP_POINTS):
            id0 = int(template_result[i])
            id1 = int(scan_result[i])
            self.icpPair.append((id0, id1))

    def runIcp(self):
        nonRigitIcp = nrIcp.NonRigidIcp(self.templateMesh, self.scanMesh, self.icpPair)
        vertexPair = nonRigitIcp.runPipeLine()
        mapFilename = self.modelFileName.replace(".obj", "Map.pkl")
        with open(mapFilename, 'w') as f:
            pickle.dump(vertexPair, f)
        print "vertex Pair done=========="
        idx = self.modelFileName.rfind('/')
        nricpfname = './NonRigitIcp/' + self.modelFileName[idx + 1:].replace(".obj", "Icp.obj")
        write_mesh(self.templateMesh, nricpfname)
        print "done===========================!!!"

    def findVertexPair(self, icpIterCnt):
        tmesh = self.templateMesh
        vertexPair = []
        scanpair = []
        P = np.array([[self.fx, 0, self.u], [0, self.fy, self.v], [0, 0, 1]])
        tVertexNum = tmesh.n_vertices()
        print "tVertexNum:", tVertexNum
        count = 0
        # ------ 计算 C_depth:template的投影坐标------------
        points = []   # points:template的三维点
        for i in range(tVertexNum):
            vh = tmesh.vertex_handle(i)
            v = tmesh.point(vh)
            point = [v[0], v[1], v[2]]
            points.append(point)
        C = np.array(points)
        C_depth = np.dot(C, P.T)
        for i in range(C_depth.shape[0]):
            z = C_depth[i][2]
            C_depth[i][0] = C_depth[i][0] / z
            C_depth[i][1] = C_depth[i][1] / z
        # ======== 计算 C_depth 完毕 ================
        # ------------------------------------------
        for i in range(tVertexNum):
            vh = tmesh.vertex_handle(i)
            tidx = vh.idx()
            v = tmesh.point(vh)
            n = tmesh.normal(vh)
            x = float(v[0])
            y = float(v[1])
            z = float(v[2])
            tn = np.array([float(n[0]), float(n[1]), float(n[2])])
            tX = int(C_depth[i][0])
            tY = int(C_depth[i][1])
            # ----- 找匹配点 -------------------
            scanset = []
            sidx_list = []
            max_dot = -1
            min_dis = 100
            sidx = -1
            # 符合向量内积要求>0.9
            for Y in range(tY - 2, tY + 3):
                for X in range(tX - 2, tX + 3):
                    sx = float(self.colormap[Y][X][0])
                    sy = float(self.colormap[Y][X][1])
                    sz = float(self.colormap[Y][X][2])
                    sn1 = float(self.scanNormalMap[Y][X][0])
                    sn2 = float(self.scanNormalMap[Y][X][1])
                    sn3 = float(self.scanNormalMap[Y][X][2])
                    sn = np.array([sn1, sn2, sn3])
                    _idx = int(self.scanidxMap[Y][X])
                    _dot = np.dot(tn, sn)
                    if _dot > 0.9:
                        scanset.append([sx, sy, sz])
                        sidx_list.append(_idx)
            # 符合向量内积要求的，进行距离最小查找
            index = -1
            if len(scanset) > 0:
                for i in range(len(scanset)):
                    dis = math.sqrt((x - scanset[i][0]) ** 2 + (y - scanset[i][1]) ** 2 + (z - scanset[i][2]) ** 2)
                    if dis < 100 and dis < min_dis:
                        min_dis = dis
                        index = i
            if index == -1:
                # vertexPair.append((tidx, sidx))
                scanpair.append([0, 0, 0])
            else:
                count += 1
                scanpair.append([scanset[index][0], scanset[index][1], scanset[index][2]])
                vertexPair.append((tidx, sidx_list[index]))
        print "count:", count
        pairname = "./globalMap/pair" + str(icpIterCnt) + ".txt"
        with open(pairname, "w") as f:
            for i in range(tVertexNum):
                vh = tmesh.vertex_handle(i)
                v = tmesh.point(vh)
                point = [v[0], v[1], v[2]]
                x = float(point[0])
                y = float(point[1])
                z = float(point[2])
                point2 = scanpair[i]
                f.write('%f,%f,%f,%f,%f,%f\n' % (x, y, z, point2[0], point2[1], point2[2]))
        print pairname, " write done"
        return vertexPair
