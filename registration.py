from serialization import loadObj
from serialization import loadTemplate, getObjList
from TemplateRegistor import TemplateRigistor as temReg
from multiprocessing import Pool


def register(objFileName):
    temp = temReg()
    temp.setScanMesh(objFileName)
    temp.setTemplate(loadTemplate())
    temp.setScanModel(loadObj(objFileName))
    temp.setPair(objFileName)
    temp.globalRT()
    temp.runIcp()


if __name__ == '__main__':
    filename = 'EarScan/04202d1189_smooth.obj'
    register(filename)
